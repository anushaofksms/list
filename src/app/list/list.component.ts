import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-list',
  templateUrl: './list.component.html',
  styleUrls: ['./list.component.css']
})
export class ListComponent implements OnInit {

  list=[];
  add:boolean
  listEmp:boolean
  taskadd:boolean
  taskone:string;
  tasktwo:string;
  task3:string;
  update:boolean;
  taskedit:boolean;
  empDetails={
    id:'',
    name:'',
    task1:'',
    task2:'',
  }
  constructor() { 
    this.add=false
    this.listEmp=true
    this.update=false
    this.taskadd=false
    this.taskedit=false
    this.list=[{id:"1",name:"anusha",task1:"register",task2:"login"},
               {id:"2",name:"anuu",task1:"other",task2:"anuu"},
               {id:"3",name:"test",task1:"other",task2:"test"},
               {id:"4",name:"user",task1:"other",task2:"user"}];
  }

  ngOnInit() {
    this.list
  }
  
  checkData(data)
  {
    this.empDetails.id=data.id
    this.taskone=data.task1;
    this.tasktwo=data.task2;
    this.task3=data.task3;
    console.log(this.empDetails.id);
  }

  addEmployee()
  {
    this.listEmp=false
    this.add=true
  }

  saveData(data)
  {
    let empData={
      id:data.value.id,
      name:data.value.name,
      task1:data.value.taskone,
      task2:data.value.tasktwo
    }
    this.list.push(empData)
    this.listEmp=true
    this.add=false
  }

  editEmployee(edit)
  {
    this.update=true
    this.listEmp=false
    this.add=false
    this.empDetails.id=edit.id,
    this.empDetails.name=edit.name,
    this.empDetails.task1=edit.task1,
    this.empDetails.task2=edit.task2

    // console.log(this.empDetails);
  }

  updateData()
  {
    let updateemp={
      id:this.empDetails.id,
      name:this.empDetails.name,
      task1:this.empDetails.task1,
      task2:this.empDetails.task2,
    }
    // console.log(updateemp);

    for (let i = 0; i < this.list.length; i++) {
      if(this.list[i].id == updateemp.id){
        //  console.log(this.list[i]);
        //  console.log(updateemp);
        this.list[i] = updateemp;
        this.list;
        this.listEmp=true
        this.add=false
        this.update=false
      }
    }
  }

  addTask()
  {
    this.taskadd=true
  }

  addNewTask(task)
  {
    for (let i = 0; i < this.list.length; i++) {
      if(this.list[i].id == this.empDetails.id){
        console.log('anuu')
        this.list[i]['task3'] = task.value.task;
      }
    }
    console.log(this.list);
  }

  removeData(data){
    for (let i = 0; i < this.list.length; i++) {
      if(this.list[i].id == data.id){
        console.log(data.id)
         this.list.splice(data.id-1 ,1);
      }
    }
}

updateTask(taskone,tasktwo)
{
  console.log(taskone)
  console.log(tasktwo)
  this.taskadd=false
  this.taskedit=true
  // this.empDetails.id=edit.id,
  // this.empDetails.name=edit.name,
  this.empDetails.task1=taskone;
  this.empDetails.task2=tasktwo;
}

addExistingTask()
{
   let editnewtsk={
     task1:this.empDetails.task1,
     task2:this.empDetails.task2
   }
   console.log(editnewtsk)

   for (let i = 0; i < this.list.length; i++) {
     console.log(this.list[i].task1)
    if(this.list[i].task1 == this.empDetails.task1 && this.list[i].task2 == this.empDetails.task2){
      this.list[i].task1 = editnewtsk.task1;
      this.list[i].task2 = editnewtsk.task2;
      this.list;
      this.listEmp=true
      this.add=false
      this.update=false
      this.taskedit=false
    }
  }
}

}
